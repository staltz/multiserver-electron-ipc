# multiserver-electron-ipc

_A [multiserver](https://github.com/ssbc/multiserver) plugin for [Electron IPC](https://electronjs.org/docs/api/ipc-main)_

```
npm install --save multiserver-electron-ipc
```

## Usage

The main thread in Electron must not do heavy processing, so we're going to use it only as a lightweight bridge between the frontend and the backend. The "frontend" is a renderer thread where you have a browser window with the DOM. The "backend" is a Node.js `worker_threads` instance, i.e. Worker. `pull-electron-ipc` only supports this architecture shape.

```mermaid
flowchart TB;
  Main <--> Worker
  Main <--> Renderer
```

To setup multiserver-electron-ipc in each of these three parts, do the following:

### Main thread

Note that you require `multiserver-electron-ipc/main`, NOT `multiserver-electron-ipc`.

```js
const {ipcMain, app, BrowserWindow} = require('electron')
const setup = require('multiserver-electron-ipc/main')
const {Worker} = require('worker_threads')

app.on('ready', () => {
  const win = new BrowserWindow({ width: 800, height: 600 })
  win.loadURL(`file://${__dirname}/index.html`)

  const worker = new Worker(SOME_KIND_OF_FILENAME);

  setup(worker, ipcMain, win.webContents);
})
```

### Worker thread

```js
const {parentPort} = require('worker_threads')
const pull = require('pull-stream')
const MultiServer = require('multiserver')
const electronIpcPlugin = require('multiserver-electron-ipc')

const ms = MultiServer([
  electronIpcPlugin({ parentPort })
])

ms.server(function(stream) {
  pull(
    stream,
    pull.map(s => s.toUpperCase()),
    stream
  )
})
```

### Renderer thread

```js
const {ipcRenderer} = require('electron')
const pull = require('pull-stream')
const MultiServer = require('multiserver')
const electronIpcPlugin = require('multiserver-electron-ipc')

const ms = MultiServer([
  electronIpcPlugin({ ipcRenderer })
])

ms.client('channel', function(err, stream) {
  pull(
    pull.values(['alice', 'bob']),
    stream,
    pull.drain(x => {
      console.log(x)  // ALICE
                      // BOB
    })
  )
})

// the address has no parameters, there is only one Electron IPC main process
ms.stringify() => "channel"
```

Note, there is no scope option, the scope is always `device`.

## Usage with [muxrpc](https://github.com/ssbc/muxrpc)

### Renderer thread

```js
// ...
ms.client('channel', function(err, stream) {
  const manifest = {
    stuff: 'source'
  }
  const client = muxrpc(manifest, null)()

  pull(
    client.stuff(),
    pull.drain(x => {
      console.log(x)  // 2
                      // 4
                      // 6
                      // 8
    })
  )

  pull(stream, client.createStream(), stream)
})
// ...
```

### Worker thread

```js
// ...
ms.server(function(stream) {
  const manifest = {
    stuff: 'source'
  }
  const server = muxrpc(null, manifest)({
    stuff: function() {
      return pull.values([2, 4, 6, 8])
    }
  })

  pull(stream, server.createStream(), stream)
})
// ...
```
