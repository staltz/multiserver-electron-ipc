const toDuplex_Server = require('pull-electron-ipc/worker');
const toDuplex_Client = require('pull-electron-ipc/renderer');

function noop() {}

module.exports = function makePlugin(opts) {
  const name = !!opts ? opts.name || 'channel' : 'channel';
  return {
    name,

    scope() {
      return 'device';
    },

    server(onConnection, onError) {
      const parentPort = !!opts ? opts.parentPort : null;
      if (!parentPort) {
        onError &&
          onError(
            new Error(
              'multiserver-electron-ipc plugin requires parentPort in ' +
                'the opts argument when starting the server'
            )
          );
        return noop;
      }

      const duplex = toDuplex_Server(parentPort);
      onConnection(duplex);
      return function closeMultiserverElectronIPC() {
        if (duplex) {
          duplex.source(true, noop);
          duplex = null;
        }
      };
    },

    client(address, cb) {
      if (address !== name) {
        throw new Error(
          'multiserver-electron-ipc ' +
            'cannot start a client for the address ' +
            address
        );
      }
      try {
        const ipcRenderer = !!opts ? opts.ipcRenderer || opts : null;
        if (!ipcRenderer) {
          throw new Error(
            'multiserver-electron-ipc plugin requires the ipcRenderer given ' +
              'in the opts argument when starting the client'
          );
        }
        const stream = toDuplex_Client(ipcRenderer);
        stream.channel = ipcRenderer;
        cb(null, stream);
      } catch (err) {
        cb(err);
      }
    },

    parse(s) {
      if (s !== name) return null;
      return name;
    },

    stringify() {
      return name;
    },
  };
};
