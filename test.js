var pull = require('pull-stream');
var test = require('tape');
var muxrpc = require('muxrpc');
var MultiServer = require('multiserver');
var EventEmitter = require('events');
var electronIpcPlugin = require('./index');

var latestRandom = 7;
var primeMult = 37;
var primeMax = 61;
function deterministicPseudoRandomNumber() {
  latestRandom = (latestRandom * primeMult) % primeMax;
  return latestRandom;
}

function mock() {
  var parentPort = new EventEmitter();
  var ipcRenderer = new EventEmitter();
  parentPort.postMessage = function(raw) {
    setTimeout(() => {
      ipcRenderer.emit('pull-electron-ipc-renderer', raw);
    }, deterministicPseudoRandomNumber() * 10);
  };
  ipcRenderer.send = function(_c, raw) {
    setTimeout(() => {
      parentPort.emit('message', raw);
    }, deterministicPseudoRandomNumber() * 10);
  };
  return {parentPort, ipcRenderer};
}

test('basic server and client work correctly', function(t) {
  var {parentPort, ipcRenderer} = mock();
  var ms1 = MultiServer([
    electronIpcPlugin({parentPort}),
  ]);

  ms1.server(function(stream) {
    pull(stream, pull.map(s => s.toUpperCase()), stream);
  });

  var ms2 = MultiServer([electronIpcPlugin({ipcRenderer})]);

  ms2.client('channel', function(err, stream) {
    t.error(err, 'no error initializing multiserver');
    pull(
      pull.values(['alice', 'bob']),
      stream,
      pull.collect(function(err2, arr) {
        t.error(err2, 'no error from worker');
        t.deepEqual(arr, ['ALICE', 'BOB'], 'data got uppercased in the worker');
        t.end();
      })
    );
  });
});

test('can use a custom name for the addresses', function(t) {
  var {parentPort, ipcRenderer} = mock();
  var ms1 = MultiServer([
    electronIpcPlugin({name: 'ipc', parentPort}),
  ]);

  ms1.server(function(stream) {
    pull(stream, pull.map(s => s.toUpperCase()), stream);
  });

  var ms2 = MultiServer([electronIpcPlugin({name: 'ipc', ipcRenderer})]);

  ms2.client('ipc', function(err, stream) {
    t.error(err, 'no error initializing multiserver');
    pull(
      pull.values(['alice', 'bob']),
      stream,
      pull.collect(function(err2, arr) {
        t.error(err2, 'no error from worker');
        t.deepEqual(arr, ['ALICE', 'BOB'], 'data got uppercased in the worker');
        t.end();
      })
    );
  });
});

test('muxrpc server and client work correctly', function(t) {
  t.plan(3);
  var manifest = {
    stuff: 'source',
  };

  var {parentPort, ipcRenderer} = mock();
  var ms1 = MultiServer([
    electronIpcPlugin({parentPort})
  ]);

  ms1.server(function(stream) {
    var server = muxrpc(null, manifest)({
      stuff: function() {
        return pull.values([2, 4, 6, 8]);
      },
    });

    pull(stream, server.createStream(), stream);
  });

  var ms2 = MultiServer([electronIpcPlugin({ipcRenderer})]);

  ms2.client('channel', function(err, stream) {
    if (err) console.error(err.stack);
    t.error(err, 'no error initializing multiserver');
    var client = muxrpc(manifest, null)();
    pull(
      client.stuff(),
      pull.collect(function(err2, arr) {
        t.error(err2, 'no error from worker');
        t.deepEqual(arr, [2, 4, 6, 8], 'got data sourced from the worker');
        t.end();
      })
    );
    pull(stream, client.createStream(), stream);
  });
});

test('forcefully exit even if some child workers are running', function(t) {
  t.end();
  process.exit(0);
});
